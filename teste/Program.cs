﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace X.Tiss.Roleta
{
    class Program
    {
        public static Logger Log = LogManager.GetCurrentClassLogger();
        public static string _nm_servico = ConfigurationManager.AppSettings["service_name"].ToString();

        static void Main(string[] args)
        {

            //{"id":"533606d3-55ce-4f39-8b50-99a66dd31f0a","prestador":"15352758-e674-492e-8013-b4eaa92ba9ee","integracao":false,"CodigoPrestador":null,"TipoCodigo":0,"SequencialTransacao":null,"Operadora":null,"Processado":false,"Data":"0001-01-01T00:00:00","Id":null,"Mensagem":null,"Arquivo":null,"Nom_Usuario_Integrador":"juliandro","Nom_Senha_Integrador":"leforte357","Nom_Usuario_WS":"hleforte","Nom_Senha_WS":"e807f1fcf82d132f9bb018ca6738a19f","Sta_Unico":false,"Protocolo":"55331946","Sta_Represamento":false,"STA_LIBERACAO_AUTOMATICA":false}
            //
            string msg = "{\"prestador\":\"15352758-E674-492E-8013-B4EAA92BA9EE\",\"id\":\"4309E1EC-47E5-48F9-8197-60F5E09F55B3\",\"integracao\":true}";
            //
            new Transmitir().Enviar(msg);

            HostFactory.Run(x =>
            {
                x.Service<Processador>(s =>
                {
                    s.ConstructUsing(name => new Processador());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });

                x.RunAsLocalSystem();

                x.SetServiceName(_nm_servico);
                x.SetDisplayName(ConfigurationManager.AppSettings["service_display_name"].ToString());
                x.SetDescription(_nm_servico + " : Serviço responsável por direcionar o processamento dos arquivos recebidos.");
            });
        }
    }
}
