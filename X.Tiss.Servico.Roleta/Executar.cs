﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X.Tiss.Content.Models;

namespace X.Tiss.Servico.Roleta
{
    public class Executar
    {
        private string _fila_limpeza_key = ConfigurationManager.AppSettings["fila_limpeza_key"].ToString();
        private static Logger Log = LogManager.GetCurrentClassLogger();
        private static string ConnString = ConfigurationManager.ConnectionStrings["EasyTiss_App"].ToString();

        ///<summary>
        ///Configuração
        /// App Config:
        ///     ConnectionString
        ///         EasyTiss_App
        ///
        ///     Setting
        ///         fila_limpeza_key

        ///         Cod_App
        ///         Cod_status
        ///         Cod_status_erro

        ///         mq_host
        ///         mq_porta
        ///         mq_usuario
        ///         mq_senha

        /// Referencias
        ///     NLog
        ///     NLog.Config
        ///     Dapper

        ///     Newtonsoft.Json
        ///     RabbitMQ.Client
        ///     X.Tiss.MQ.Utilitario
        ///     X.Tiss.Content
        ///</summary>
        public void Enviar(string msg)
        {
            Historico historico = new Historico();
            var arquivo = JsonConvert.DeserializeObject<ArquivoResult>(msg);
            Recebimento rec = new Recebimento();
            Int16 status = Convert.ToInt16(ConfigurationManager.AppSettings["Cod_status"]);
            Integrador _integrador = new Integrador();

            try
            {
                Log.Info("X.Tiss.Servico.Roleta.Integrar - Enviar{msg: " + msg.Substring(0, 50) + "...}: Inicio do processamento da mensagem");
                _integrador = _integrador.Carregar(arquivo.id, ConnString);
                Notificar(_integrador != null && _integrador.Cod_Integradora > 0 ? _integrador.Nom_Fila : _fila_limpeza_key, arquivo.prestador.ToString(), arquivo.id.ToString(), _integrador);
                historico.Incluir(arquivo.id, Convert.ToInt32(ConfigurationManager.AppSettings["Cod_App"].ToString()), (_integrador != null && _integrador.Cod_Integradora > 0), _integrador != null && _integrador.Cod_Integradora > 0 ? null  : "A Fila para esse arquivo não foi encontrada.", ConnString);
                Log.Info("X.Tiss.Servico.Roleta.Integrar - Enviar: " + (_integrador.Cod_Integradora > 0 ? _integrador.Nom_Fila : "A Fila para esse arquivo não foi encontrada."));

            }
            catch (Exception ex)
            {
                Log.Error("X.Tiss.Servico.Roleta.Integrar - Enviar: Falha no processamento da mensagem, erro: " + ex.Message);
                status = Convert.ToInt16(ConfigurationManager.AppSettings["Cod_status_erro"]);
                historico.Incluir(arquivo.id, Convert.ToInt32(ConfigurationManager.AppSettings["Cod_App"].ToString()), false, (ex.Message.Length > 199 ? ex.Message.Substring(0, 200) : ex.Message), ConnString);
            }

            if (_integrador != null && _integrador.Cod_Integradora > 0)
                rec.Atualizar_Status(arquivo.id, status, ConnString);
        }

        private void Notificar(string nm_fila, string prestador, string id, Integrador integrador)
        {
            string msg = JsonConvert.SerializeObject(new
            {
                prestador = prestador
            ,
                id = id
            ,
                Nom_Usuario_Integrador = integrador != null && integrador.Cod_Integradora > 0 ? integrador.Nom_Usuario_Integrador : "" 
            ,
                Nom_Senha_Integrador = integrador != null && integrador.Cod_Integradora > 0 ? integrador.Nom_Senha_Integrador : ""
            ,
                Nom_Usuario_WS = integrador != null && integrador.Cod_Integradora > 0 ? integrador.Nom_Usuario_WS : ""
            ,
                Nom_Senha_WS = integrador != null && integrador.Cod_Integradora > 0 ? integrador.Nom_Senha_WS : ""
            ,
                Sta_Unico = integrador != null && integrador.Cod_Integradora > 0 ? integrador.Sta_Unico.ToString() : ""
            });

            new X.Tiss.MQ.Utilitario.MQConnect( ConfigurationManager.AppSettings["mq_host"]
                                              , Convert.ToInt32(ConfigurationManager.AppSettings["mq_porta"])
                                              , ConfigurationManager.AppSettings["mq_usuario"]
                                              , ConfigurationManager.AppSettings["mq_senha"]
                                              ).Mensagem(nm_fila, nm_fila, msg);
                        
        }
    }
}
